const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
const Notify = require('../models/notification');
const config = require('../config');
const uuid = require('uuid');
const moment = require('moment');

//Register
router.post('/register', (req, res, next) => {

  if(!/^([a-zA-Z0-9_]+)$/.test(req.body.username) || !/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(req.body.phone)) {
    return res.status(400).json({ success: false, msg: 'Stop trying to hack my usernames!' });
  }

//This stops people from making blank requests and crashing the server and requires username and phone on post request
  if (!req.body.username || !req.body.phone || !req.body.password || !req.body.name ) {
  return res.status(400).json({ success: false, msg: 'Please fill out all required items' });
};

if (req.body.username.length > 15 || req.body.phone.length > 14 || req.body.password.length > 50 || req.body.name.length > 40 ) {
return res.status(400).json({ msg: 'Field too long. Are you trying to hack?' });
};


  User.getUserByUsername(req.body.username.toLowerCase(), (err, user) => {
    if(err) throw err;
    //check
    if(user) {
      return res.json({ msg: 'Username already taken!' });
    } else {
      let newUser = new User({
        name: req.body.name,
        phone: req.body.phone,
        uuid: `${req.body.username.toLowerCase()}+${uuid.v4()}`,
        created: moment().format(),
        username: req.body.username.toLowerCase(),
        password: req.body.password,
        avatar: 'https://i.imgur.com/YfyWjq4.gif',
        bio: 'Why yes I am a groupie, how did you know?',
        location: 'A magic place'
      });

      User.addUser(newUser, (err, user) => {
        if(err){
          res.json({success: false, msg:'Failed to register user'});
        } else {
          res.json({success: true, msg:'User registered!'});
        }
      })
    }
  });

});

//username Check (for registering check)
router.post('/check', (req, res, next) => {

  User.getUserByUsername(req.body.username.toLowerCase(), (err, user) => {
    if(err) throw err;
    if(user) {
      return res.json({ success:false, msg: 'Username already taken!' });
    } else {
      return res.json({success:true, msg: 'Name available!'})
    }
  })
});


//Authenticate
router.post('/authenticate', (req, res, next) => {
  const username = req.body.username.toLowerCase();
  const password = req.body.password;
  const keep = req.body.keep;

  if(!req.body.username || !req.body.password) {
    return res.json({success: false, msg: 'Fill out the boxes!'});
  }

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found!'});
    }

    User.comparePassword(password, user.password, (err, isMatch) => {
      if(err) throw err;

      if(keep === true){
        exp = { expiresIn: 999999999 }
      } else {
        exp = { expiresIn: 604800 }
      }


      if(isMatch){
        const token = jwt.sign({data: {username: user.username, uuid: user.uuid, id: user._id}}, config.secret, exp);

          res.json({
            success: true,
            token: `Bearer ${token}`,
            user: {
              id: user._id,
              name: user.name,
              uuid: user.uuid,
              username: user.username,
              phone: user.phone
            }
          })
      } else {
        return res.json({success: false, msg: 'Invalid password!'});
      }
    });
  });
});

//Password Change
router.put('/passwordchange', passport.authenticate('jwt', {session:false}), (req, res, next) => {

  if (!req.body.newpassword || !req.body.password ) {
  return res.status(400).json({success: false, msg: 'Please fill out all required items' });
};

  const username = req.user.username;
  const user = req.body;
  const password = req.body.password;
  const id = req.user._id;
  const newPassword = req.body.newpassword;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found!'});
    }

  User.comparePassword(password, user.password, (err, isMatch) => {
    if(err) throw err;
    if(isMatch){
      User.updatePassword(id, newPassword, {}, (err, newPassword) => {
        if(err){
          throw err;
        }
        res.json({success: true, msg: "updated!"});
      });

    } else {
      return res.json({success: false, msg: 'Invalid password!'});
    }
  })
})
})

//Profile, add what you want here. Program the /edit function with it on front end
router.get('/profile', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  res.json({user: req.user});
});

//Get by Username
router.get('/profile/:username', (req, res, next) => {
	User.getUserByUsername(req.params.username.toLowerCase(), (err, user) => {
    if(!user) {
      return res.json({success: false, msg: 'No user found!'});
    }

		if(err){
			throw err;
		}
		res.json({success: true, user: {name: user.name, username: user.username, bio: user.bio, avatar: user.avatar, phone: user.phone, followers: user.followers, following: user.following}});
	});
});

//The delete route
//LEAVE THIS AS .put!!! Angular shits itself otherwise.
router.put('/delete', passport.authenticate('jwt', {session:false}), (req, res, next) => {

  if (!req.body.password ) {
  return res.status(400).json({ msg: 'Please fill out all required items' });
};

  const username = req.user.username;
  const user = req.body;
  const password = req.body.password;
  const id = req.user._id;

  User.getUserByUsername(username, (err, user) => {
    if(err) throw err;
    if(!user){
      return res.json({success: false, msg: 'User not found!'});
    }

  User.comparePassword(password, user.password, (err, isMatch) => {
    if(err) throw err;
    if(isMatch){

      User.deleteById(req.user._id, (err, user) => {
        if(err){
          throw err;
        }
        res.json({success: true, msg: 'User deleted!'});
      });

    } else {
      return res.json({success: false, msg: 'Invalid password!'});
    }
  })
})
})

//Allow users to update their info
router.put('/update', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let user = req.body;
    let id = req.user._id;


    //fill this out later too
    if(!req.body.bio || !req.body.name.length || !req.body.avatar || !req.body.location ){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.bio.length > 150 || req.body.name.length > 40 || req.body.avatar.length > 50 || req.body.location.length > 140 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

	User.updateUser(id, user, {}, (err, user) => {
		if(err){
			throw err;
		}
		res.json({success: true, msg: "updated!"});
	});
});

//Follow someone or something I dunno
router.put('/follow', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let follow = { username: req.user.username, target: req.body.target}

    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {
        //check if you're in their follow list
  for (let i = 0; i < user.followers.length; i++) {
      if (user.followers[i].user === req.user.username) {
          return res.json({success: false, msg: 'You already follow them!'});
      }
  }
  //same thing but with blocked lol
  for (let i = 0; i < user.blocked.length; i++) {
      if (user.blocked[i].user === req.user.username) {
          return res.json({success: false, msg: 'This user has blocked you!'});
      }
  }

  for (let i = 0; i < req.user.blocked.length; i++) {
      if (req.user.blocked[i].user === req.body.target) {
          return res.json({success: false, msg: 'Unblock them first!'});
      }
  }

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

	User.followUser(id, follow, {}, (err, follow) => {
		if(err){
			throw err;
		}

    });

    //literally creating the notification within this bitch lmao
    let newNotify = new Notify({
      notificationType: "Follow",
      from: req.user.username,
      seen: false,
      username: req.body.target,
      date: moment().format(),
      createAt: Date.now()
    });

    User.addFollowUser(id, follow, {}, (err, follow) => {
      if(err){
        throw err;
      }
      res.json({success: true, msg: "Followed!"});

      //I'm gonna try something hacky because something is breaking
      follow = { username: req.user.username, target: req.body.target}

      Notify.removeFollowNotification(id, follow, {}, (err, follow) => {
        if(err){
          throw err;
        }
      })

      //slap that fresh new notification straight into the addfollow function. Why not?
      Notify.addNotification(newNotify, (err, notify) => {
        if(err){
          throw err;
        }
        });

  });


  }});

});



//match. Just gonna copy follow basically.
//Follow someone or something I dunno
router.put('/match', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let match = { username: req.user.username, target: req.body.target}

    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {
        //check if you're in their follow list
  for (let i = 0; i < user.matches.length; i++) {
      if (user.matches[i].user === req.user.username) {
          return res.json({success: false, msg: 'You already matched them!'});
      }
  }

  for (let i = 0; i < user.matchRequests.length; i++) {
      if (user.matchRequests[i].user === req.user.username) {
          return res.json({success: false, msg: 'You already sent them a request!'});
      }
  }
  //same thing but with blocked lol
  for (let i = 0; i < user.blocked.length; i++) {
      if (user.blocked[i].user === req.user.username) {
          return res.json({success: false, msg: 'This user has blocked you!'});
      }
  }

  for (let i = 0; i < req.user.blocked.length; i++) {
      if (req.user.blocked[i].user === req.body.target) {
          return res.json({success: false, msg: 'Unblock them first!'});
      }
  }

  for (let i = 0; i < req.user.swerves.length; i++) {
      if (req.user.swerves[i].user === req.body.target) {
          return res.json({success: false, msg: 'You can\'t match someone you swerved, hacker!'});
      }
  }

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

    //make a scenario for if they're already requesting you HERE

    for (let i = 0; i < user.matchSends.length; i++) {
        if (user.matchSends[i].user === req.user.username) {
          //I gotta make two functions to remove this user from the other person's request sent, and remove them from your request inbox. Also add eachother to matches.


          //now make sure that you remove all match requests/sends and whatnot from them and yourself
          User.unmatchSend(id, match, {}, (err, match) => {
            if(err){
              throw err;
            }
            });

            User.removeMatchSendOther(id, match, {}, (err, match) => {
              if(err){
                throw err;
              }
              });

              User.unmatchRequest(id, match, {}, (err, match) => {
                if(err){
                  throw err;
                }
                });

                User.removeMatchRequestOther(id, match, {}, (err, match) => {
                  if(err){
                    throw err;
                  }
                  });



          User.matchUser(id, match, {}, (err, match) => {
            if(err){
              throw err;
            }

            });

            User.matchOther(id, match, {}, (err, match) => {
              if(err){
                throw err;
              }


          });
          return res.json({success: true, msg: 'You guys matched! :)'});
        }
    }

    //end

	User.requestMatch(id, match, {}, (err, match) => {
		if(err){
			throw err;
		}

    });

    User.requestMatchOther(id, match, {}, (err, match) => {
      if(err){
        throw err;
      }
      res.json({success: true, msg: "Match request sent!"});
  });

  }});
});











router.get('/notifications', passport.authenticate('jwt', {session:false}), (req, res, next) => {
	Notify.getUserByUsername(req.user.username, (err, notify) => {
		if(err){
			throw err;
		}
		res.json(notify);
	});
});

router.get('/newNotifications', passport.authenticate('jwt', {session:false}), (req, res, next) => {
	Notify.getUnseenByUsername(req.user.username, (err, notify) => {
		if(err){
			throw err;
		}
		res.json(notify);
	});
});

//Technically since we're not inputting anything but the token, because fuck angular.
router.get('/markseen', passport.authenticate('jwt', {session:false}), (req, res, next) => {
	Notify.markAllSeen(req.user.username, (err, notify) => {
		if(err){
			throw err;
		}
		res.json({success:true, msg:"notifications cleared!"});
	});
});

//I HAVE TO USE PUT HERE INSTEAD OF GET BECAUSE FUCK ANGULAR'S SHITTY HTTP SYSTEM
router.put('/followers', (req, res, next) => {
  let followers = req.body

	User.getFollowersByUsername(followers, (err, followers) => {

    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

		if(err){
			throw err;
		}
		res.json(followers);
	});
});

router.put('/following', (req, res, next) => {
  let following = req.body

	User.getFollowingByUsername(following, (err, following) => {

    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

		if(err){
			throw err;
		}
		res.json(following);
	});
});

//unfollow now... fuck me that last one was long and boring
router.put('/unfollow', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let follow = { username: req.user.username, target: req.body.target}


    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

	User.unfollowUser(id, follow, {}, (err, follow) => {
		if(err){
			throw err;
		}

    });

    User.addUnfollowUser(id, follow, {}, (err, follow) => {
      if(err){
        throw err;
      }
      res.json({success: true, msg: "Unfollowed!"});

	});


}});

});

router.put('/unmatch', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let match = { username: req.user.username, target: req.body.target}


    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

	User.unmatchUser(id, match, {}, (err, match) => {
		if(err){
			throw err;
		}

    });

    User.removeMatchOther(id, match, {}, (err, match) => {
      if(err){
        throw err;
      }
      res.json({success: true, msg: "unmatched!"});

	});
}});
});

router.put('/unrequest_match', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let match = { username: req.user.username, target: req.body.target}


    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

	User.unmatchSend(id, match, {}, (err, match) => {
		if(err){
			throw err;
		}

    });

    User.removeMatchRequestOther(id, match, {}, (err, match) => {
      if(err){
        throw err;
      }
      res.json({success: true, msg: "Match request removed!"});

	});
}});
});

router.put('/swerve', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let match = { username: req.user.username, target: req.body.target}


    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }

    for (let i = 0; i < req.user.matches.length; i++) {
        if (req.user.matches[i].user === req.body.target) {
            return res.json({success: false, msg: 'You can\'t swerve someone you\'re already matched with, hacker!'});
        }
    }

    for (let i = 0; i < req.user.swerves.length; i++) {
        if (req.user.swerves[i].user === user.username) {
            return res.json({success: false, msg: 'You already swerved them!'});
        }
    }

    User.removeMatchSendOther(id, match, {}, (err, match) => {
  		if(err){
  			throw err;
  		}

      });

      User.unmatchRequest(id, match, {}, (err, match) => {
    		if(err){
    			throw err;
    		}

        });

	User.unmatchSend(id, match, {}, (err, match) => {
		if(err){
			throw err;
		}

    });

    User.removeMatchRequestOther(id, match, {}, (err, match) => {
      if(err){
        throw err;
      }

	});

  User.swerveUser(id, match, {}, (err, match) => {
    if(err){
      throw err;
    }

    });
    //let's just make it do swerve to both people. It doesn't fucking matter, once swerved people can't unswerve.
    User.swerveUserOther(id, match, {}, (err, match) => {
      if(err){
        throw err;
      }
        res.json({success: true, msg: `You swerved ${user.username}`});
      });


}});
});












//this shit is getting out of hand lmao
router.put('/block', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let block = { username: req.user.username, target: req.body.target}

    User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
      if(err) throw err;
      //check
      if(!user) {
        return res.json({ msg: 'User doesn\'t exist!' });
      } else {
  for (let i = 0; i < req.user.blocked.length; i++) {
      if (req.user.blocked[i].user === req.body.target) {
          return res.json({success: false, msg: 'You already blocked them!'});
      }
  }

  if(req.body.target === req.user.username) {
    return res.json({success: false, msg: 'You can\'t block yourself stupid!'});
  }

    //fill this out later too
    if(!req.body.target){
      return res.json({success: false, msg: 'No blanks!'});
    }

    //length for update, add the rest later
    if(req.body.target.length > 15 ){
      return res.json({success: false, msg: 'FIELD TOO LONG!'});
    }
    User.blockUser(id, block, {}, (err, block) => {
      if(err){
        throw err;
      };
    });

    User.unfollowUser(id, block, {}, (err, block) => {
      if(err){
        throw err;
      }
      });

      User.unfollowerUser(id, block, {}, (err, block) => {
        if(err){
          throw err;
        }
        });

        User.addUnfollowerUser(id, block, {}, (err, block) => {
          if(err){
            throw err;
          }
          });

      User.addUnfollowUser(id, block, {}, (err, block) => {
        if(err){
          throw err;
        }
        res.json({success: true, msg: "User blocked!"});

    });
  }
  })

  });

  router.put('/unblock', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let id = req.user._id;
    let username = req.user.username;
    let block = { username: req.user.username, target: req.body.target}


      User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
        if(err) throw err;
        //check
        if(!user) {
          return res.json({ msg: 'User doesn\'t exist!' });
        } else {

      //fill this out later too
      if(!req.body.target){
        return res.json({success: false, msg: 'No blanks!'});
      }

      //length for update, add the rest later
      if(req.body.target.length > 15 ){
        return res.json({success: false, msg: 'FIELD TOO LONG!'});
      }

  	User.unblockUser(id, block, {}, (err, block) => {
  		if(err){
  			throw err;
  		}
        res.json({success: true, msg: "Unblocked!"});
      });

  }});

  });


//Update ID. This is to force a logout. Don't ask, JWT stuff...
router.put('/force', passport.authenticate('jwt', {session:false}), (req, res, next) => {
    let user = req.user;
    let id = req.user._id;

	User.updateId(id, user, {}, (err, user) => {
		if(err){
			throw err;
		}
		res.json({success: true, msg: "updated!"});
	});
});

module.exports = router;
