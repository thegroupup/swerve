const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config')
const moment = require('moment');
const Message = require('../models/message');
const User = require('../models/user');



//Create event
router.post('/send', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  //check that all required items are filled routes, fix this when you wake up
  if (!req.body.body || !req.body.target ) {
  return res.status(400).json({ msg: 'Please fill out all required items' });
};

  if(req.body.body.length > 2500) {
    return res.status(400).json({ msg: 'Message too long! 2500 characters or less per message.'})
  }

User.getUserByUsername(req.body.target.toLowerCase(), (err, user) => {
  if(err) throw err;
  //check
  if(!user) {
    return res.json({ msg: 'User doesn\'t exist!' });
  } else {

    for (let i = 0; i < user.blocked.length; i++) {
        if (user.blocked[i].user === req.user.username) {
            return res.json({success: false, msg: 'This user has blocked you!'});
        }
    }

    for (let i = 0; i < req.user.blocked.length; i++) {
        if (req.user.blocked[i].user === req.body.target) {
            return res.json({success: false, msg: 'Unblock them first!'});
        }
    }


    let part = [`${req.body.target}`, `${req.user.username}`];

    let newMessage = new Message({
      from: req.user.username,
      username: req.body.target,
      identifier: part.sort().toString(),
      body: req.body.body,
      date: moment().format(),
      createAt: Date.now()
    });

    let id = req.user._id;

    Message.addMessage(newMessage, (err, message) => {
      if(err){
        res.json({success: false, msg:'Failed to send message'});
      } else {
        res.json({success: true, msg:newMessage.body, from: newMessage.from, to: newMessage.username});

        //this is all for the LatestMessage feature so they can see their message list.
        User.removeLatestMessage(id, newMessage, {}, (err, message) => {
          if(err){
            throw err;
          }
          User.addLatestMessage(id, newMessage, {}, (err, message) => {
            if(err){
              throw err;
            }
          })
        })

        User.removeLatestMessageYourself(id, newMessage, {}, (err, message) => {
          if(err){
            throw err;
          }
          User.addLatestMessageYourself(id, newMessage, {}, (err, message) => {
            if(err){
              throw err;
            }
          })
        })



      }
      });
  }
});
});

//a genius way to get all messages to and from someone for both sides. Gotta alphabetize it though.
router.get('/m/:target', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  let part = [`${req.params.target}`, `${req.user.username}`];
	Message.getUserByIdentifier(part.sort().toString(), (err, message) => {
		if(err){
			throw err;
		}
		res.json(message);
	});
});

router.get('/last/:target', passport.authenticate('jwt', {session:false}), (req, res, next) => {
  let part = [`${req.params.target}`, `${req.user.username}`];
	Message.getUserByIdentifierLast(part.sort().toString(), (err, message) => {
		if(err){
			throw err;
		}
		res.json(message);
	});
});

//Exort the whole fucking router lmao
module.exports = router;
