const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config');



//Connect to Database
mongoose.connect(config.db);

//On Connection
mongoose.connection.on('connected', () => {
  console.log(`Connected to database ${config.db}`)
});

mongoose.connection.on('error', (err) => {
  console.log(`Database Error: ${err}`)
});

const app = express();

//CORS Middleware
app.use(cors());

//call in routes
const users = require('./routes/users');
const messages = require('./routes/messages');

//port Number from config.js
const port = config.port;


if(config.useNginx == false) {
app.use(express.static(path.join(__dirname, 'public')))
};

//Body Parser Middleware
app.use(bodyParser.json());

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

require('./passport')(passport);

app.use('/api/users', users);
app.use('/api/messages', messages);

app.get('/', (req, res) => {
  res.send('Invalid Endpoint!');
});

if(config.useNginx == false){
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'public/index.html'));
});
};
//Start Server. This makes it work with sockets.
const server = app.listen(port, () => {
  console.log(`Server started on port ${port}`)
})

//nice little hack to add sockets in without including http module, Just express...
const io = require('socket.io').listen(server);
//let's put all the socket shit into one file to run after it's up.
require('./socket.js')(io);
