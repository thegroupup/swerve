const jwtAuth = require('socketio-jwt-auth');
const config = require('./config');
const User = require('./models/user');

module.exports = io => {

  let connectedUsers = {};
  let searchingUsers = {};

  io.use(jwtAuth.authenticate({
    secret: config.secret,    // required, used to verify the token's signature
    succeedWithoutToken: true
  }, function(payload, done) {
    // you done callback will not include any payload data now
    // if no token was supplied
    if (payload && payload.data.id) {
      //un-note the below line if you wanna show their payload info for whatever reason
      //console.log(payload)
      User.getUserById({_id: payload.data.id}, function(err, user) {
        if (err) {
          // return error
          return done(err);
        }
        if (!user) {
          // return fail with an error message
          return done(null, false, 'user does not exist');
        }
        // return success with a user info
        return done(null, user);
      });
    } else {
      return done() // in your connection handler user.logged_in will be false
    }
  }));

  //mockup. Delete this later.
  let waiting = []
  let inCall = []

  compareDist = (lat1, lon1, lat2, lon2) => {
    let R = 6371e3; // metres
  let φ1 = lat1 * Math.PI/180; // φ, λ in radians
  let φ2 = lat2 * Math.PI/180;
  let Δφ = (lat2-lat1) * Math.PI/180;
  let Δλ = (lon2-lon1) * Math.PI/180;

  let a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
            Math.cos(φ1) * Math.cos(φ2) *
            Math.sin(Δλ/2) * Math.sin(Δλ/2);
  let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

  let d = R * c; // in metres
  //returns how far it is in kilometers
  return d / 1000
  }


//all this to run after authentication stuff.
  io.on("connection", socket => {
    // Log whenever a user connects
    let countdownTime = 0;

    //console log when a user connects with their socket id

    if (!connectedUsers[socket.request.user.username]) {
      //we're doing set now in the object per each username.
      //this will account for ALL connections by that user so they can multi-tab or multi-device with the same login and sockets won't override eachother.
            connectedUsers[socket.request.user.username] = new Set();
        }

        //adds the ID to the user's name if they're already on
        connectedUsers[socket.request.user.username].add(socket.id);


    console.log(`${socket.request.user.username}(${socket.id}) connected`);
    //add them to the connectedUsers object we created up top
    //connectedUsers[socket.request.user.username] = socket.id;

    //console log all connected users
    console.log(connectedUsers)

    console.log(`currently ${Object.keys(connectedUsers).length} users online!`)


    // now you can access user info through socket.request.user
    // socket.request.user.logged_in will be set to true if the user was authenticated
    socket.emit('success', {
      message: 'Connection success! Logged in',
      user: socket.request.user
    });

    let userSwerves = socket.request.user.swerves;
    let userMatches = socket.request.user.matches;


    // Log whenever a client disconnects from our websocket server
    socket.on("disconnect", function() {

      if (connectedUsers[socket.request.user.username]) {
           connectedUsers[socket.request.user.username].delete(socket.id);
           if (connectedUsers[socket.request.user.username].size === 0) {
               delete connectedUsers[socket.request.user.username];
           }
       }

      console.log(`${socket.request.user.username}(${socket.id}) disconnected`);
      //delete connectedUsers[socket.request.user.username]
      console.log(connectedUsers)
      console.log(`currently ${Object.keys(connectedUsers).length} users online!`)




      //now write a disconnect for loop for waiting to remove the user.
      for (i = 0; i < waiting.length; i++) {
        if (socket.id == waiting[i].socketId) {
          //change this console log to add both users to a chat room, remove person[i] from the array
          waiting.splice(i, 1)
      console.log(`${socket.request.user.username} (${socket.id}) left the waiting room`);
      console.log(waiting)
      break;
      } else {
        console.log('user not in Swerve')
      }

        }

        for (i = 0; i < inCall.length; i++) {
          if (socket.id == inCall[i].socketId) {
            //change this console log to add both users to a chat room, remove person[i] from the array
        console.log(`${socket.request.user.username} (${socket.id}) ended swerve call`);
        console.log(inCall)


        io.in(inCall[i].room).clients((error, clients) => {
          if (error) throw error;
          console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
        });

        io.to(socket.id).emit('disconnectRoom', {room: inCall[i].room, disconnector: socket.request.user.username});
        countdownTime = 0
        inCall.splice(i, 1);
        break;
        } else {
          console.log('user not in swervecall')
        }

          }
          //gotta run it twice to get the other user
          for (i = 0; i < inCall.length; i++) {
            if (inCall[i].peerId == socket.id) {
              //change this console log to add both users to a chat room, remove person[i] from the array
          console.log(`${inCall[i].username} (${inCall[i].socketId}) was disconnected`);
          console.log(inCall)

          //make the peer leave :^(
          io.sockets.connected[inCall[i].socketId].leave(inCall[i].room);

          io.in(inCall[i].room).clients((error, clients) => {
            if (error) throw error;
            console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
          });

          io.to(inCall[i].socketId).emit('disconnectRoom', {room: inCall[i].room, disconnector: socket.request.user.username});
          inCall.splice(i, 1)
          break;
          } else {
            console.log('user no longer in swervecall')
          }

            }



    });

    // When we receive a 'message' event from our client, print out
    // the contents of that message and then echo it back to our client
    // using `io.emit()`
    socket.on("message", data => {

      //looks like we need a for loop here to send it to EVERYONE in the set of that username.
      //this is to deal with multiple tab or device logins
      //the if is so if they're not online the whole fucking server doesn't crash
      //added in the second part of the if statement on the offchance someone is cringe enough to recreate the entire socket system to somehow spoof messages, by making sure they can't spoof data.from by matching it with the socket.request.user. Now they have to have the token 10000%
      if(data.to in connectedUsers && socket.request.user.username == data.from) {
              console.log(`${data.from} said ${data.message} to ${data.to}`);
        for(let userSet of connectedUsers[data.to]) {
            io.to(userSet).emit("message", {type: "new_message", text: data.message, from: data.from, to: data.to});
        }
      }
    });

    socket.on("signalSend1", data => {
      socket.to(data.room).emit('signalSend1', {room: data.room, rtcId: data.rtcId});
      //console.log(data)

    });

    socket.on("signalSend2", data => {
      socket.to(data.room).emit('signalSend2', {room: data.room, rtcId: data.rtcId});
      //console.log(data)

    });

    socket.on("swerveMessage", data => {
      console.log(data);
      socket.to(data.room).emit('swerveMessage', {username: socket.request.user.username, room: data.room, message: data.message});
    });

    socket.on("clearCountdown", () => {
      countdownTime = 0
    });


    socket.on("search-p2p", data => {
      let available = true;
      let smCheck = false;

      //Step 1: Add and create a searching function to connect the users
      if(isNaN(data.prefDist) || data.prefDist > 101 || data.prefDist < 0){
        data.prefDist = 25
      }

      console.log(`${socket.request.user.username} is searching for people within ${data.prefDist} kilometers of ${data.lat},${data.lon}`)

        for (i = 0; i < inCall.length; i++) {
          if(socket.id == inCall[i].socketId){
            console.log(`You're already in a call!`);
            available = false;
            break;
          }
        }

        console.log(available)


      if(available !== false){

        if(waiting.length == 0){
          console.log('no matches found');
          io.to(socket.id).emit('waiting', {msg: `no matches found, we'll connect you when someone finds you. You've been added to the waiting room.`});
          waiting.push({username:socket.request.user.username, socketId:socket.id, lat:data.lat, lon:data.lon, prefDist:data.prefDist});
        }



        for (i = 0; i < waiting.length; i++) {
          let matchFound = false;
          let endOfLine = false;

            //this whole thing should be the join room function, it checks distance. Then I'll put an API check if swerved or matched. I'm gonna have to start using some async programming now because API checks are slower than the rest my code.
            //I need the API callback though. socket.request.user shit doesn't update until the user logs out and back in. I can't be having them rematch same users they just matched/swerved in a single session
          if (compareDist(data.lat, data.lon, waiting[i].lat, waiting[i].lon) < data.prefDist && compareDist(data.lat, data.lon, waiting[i].lat, waiting[i].lon) < waiting[i].prefDist && waiting[i].socketId !== socket.id) {
            //change this console log to add both users to a chat room, remove person[i] from the array


            //new test: We're gonna call swerves/matches from Socket.request, BUT we're going to make it a variable and just see if we can add anyone the user swerves or matches to it.
            //in theory it should work like messages? An initial API call but update the shit with sockets.



            console.log(userSwerves)


            if(waiting[i].username == waiting[waiting.length - 1].username && compareDist(data.lat, data.lon, waiting[waiting.length - 1].lat, waiting[waiting.length - 1].lon) > data.prefDist){
              endOfLine = true;
            }

            let peer = waiting[i];
              //this will be the API match and swerve Check. hence S/M Check variable I just created.
              let matched = false;
               let swerved = false;


               if(userMatches.length > 0){
                 for (e = 0; e < userMatches.length; e++){

                   if(userMatches[e].user == peer.username){
                     console.log('already matched them')
                     matched = true;
                     break;
                   } else if(e == userMatches.length - 1 || userMatches.length == 0){
                     console.log('nope didnt match them');
                     matched = false;

                   }
                 }
               }

               if(userSwerves.length > 0){
                 for (g = 0; g < userSwerves.length; g++){

                   if(userSwerves[g].user == peer.username){
                     console.log('already swerved')
                     swerved = true;
                     break;
                   } else if(g == userSwerves.length - 1 || userSwerves.length == 0){
                     //copy paste the functions
                     console.log('didnt swerve them either')
                     swerved = false;

                     //end the function
                   }
                 }
               }


               if(matched == false && swerved == false){
                 console.log('I THINK I GOT IT')
                 //this whole thing should be it's own function really
                       let alphaRoom = [socket.id, peer.socketId].sort();
                       socket.join(alphaRoom.toString());
                       io.sockets.connected[peer.socketId].join(alphaRoom.toString());
                       //sockets[waiting[i].socketId].join(alphaRoom.toString());

                   console.log(`${socket.request.user.username}(${socket.id}) connected with ${peer.username}(${peer.socketId})`);

                   //lastly we should alert the users that they've been summoned to the room, right?
                   //we can do something in the frontend with this like build the chatroom.
                   io.to(socket.id).emit('connectRoom', {room: alphaRoom.toString(), peer: peer.username, distance: compareDist(data.lat, data.lon, peer.lat, peer.lon)});
                   io.to(peer.socketId).emit('connectRoom', {room: alphaRoom.toString(), peer: socket.request.user.username, distance: compareDist(data.lat, data.lon, peer.lat, peer.lon)});
                   io.to(socket.id).emit('startCam', {room: alphaRoom.toString()});

                     //timer attempt
                     countdownTime = 121
                      let myInterval = setInterval(function(){

                     countdownTime--,
                     //console.log(countdownTime);
                     io.in(alphaRoom.toString()).emit('roomTime', {time: countdownTime});
                        if(countdownTime < 1){
                            clearInterval(myInterval)

                            //yes I really fucking have to do the god damn fucking disconnect shit in here too.

                            for (i = 0; i < inCall.length; i++) {
                              if (socket.id == inCall[i].socketId) {
                                //change this console log to add both users to a chat room, remove person[i] from the array
                            console.log(`${socket.request.user.username} (Server Timer) ended swerve call`);
                            console.log(inCall)

                            io.sockets.connected[socket.id].leave(inCall[i].room);

                            io.in(inCall[i].room).clients((error, clients) => {
                              if (error) throw error;
                              console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
                            });

                            io.to(socket.id).emit('disconnectRoom', {room: inCall[i].room, disconnector: `Server Timer`});
                            inCall.splice(i, 1);
                            countdownTime = 0;
                            break;
                            } else {
                              console.log('user not in swervecall')
                            }

                              }
                              //gotta run it twice to get the other user
                              for (i = 0; i < inCall.length; i++) {
                                if (inCall[i].peerId == socket.id) {
                                  //change this console log to add both users to a chat room, remove person[i] from the array
                              console.log(`${inCall[i].username} (${inCall[i].socketId}) was disconnected`);
                              console.log(inCall)

                              //make the peer leave :^(
                              io.sockets.connected[inCall[i].socketId].leave(inCall[i].room);

                              io.in(inCall[i].room).clients((error, clients) => {
                                if (error) throw error;
                                console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
                              });

                              io.to(inCall[i].socketId).emit('disconnectRoom', {room: inCall[i].room, disconnector: `Server Timer`});
                              inCall.splice(i, 1)
                              break;
                              } else {
                                console.log('user no longer in swervecall')
                              }

                                }

                        }
                     }, 1000);


                   console.log(waiting)

                   inCall.push({username:socket.request.user.username, socketId:socket.id, room: alphaRoom.toString(), peer: peer.username, peerId: peer.socketId});
                   inCall.push({username: peer.username, socketId: peer.socketId, room: alphaRoom.toString(), peer: socket.request.user.username, peerId: socket.id});
                   console.log("the following people are in call:")
                   console.log(inCall)

                         io.in(alphaRoom).clients((error, clients) => {
                   if (error) throw error;
                   console.log(`Clients in ${alphaRoom} are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
                   });


                   console.log('found them a match')
                   smCheck == true;
                   waiting.splice(i, 1)
                   console.log(waiting)
                   break;

                   //end match check



               }









            }
            //end of the distance and matching shit

    //start waiting room function
    //note: I'm not sure IF it accounts for distance or not? I'd have to run some tests once it's live with a friend out of distance range. 
    if( waiting[i].username == waiting[waiting.length - 1].username && smCheck == false ) {
      //change this to add the person's info to the array
      console.log('no matches found');
      io.to(socket.id).emit('waiting', {msg: `no matches found, we'll connect you when someone finds you. You've been added to the waiting room.`});
      waiting.push({username:socket.request.user.username, socketId:socket.id, lat:data.lat, lon:data.lon, prefDist:data.prefDist});
      console.log(waiting)
      break;

    }
    //end waiting room function




        }}

    });


    //leave the call
    socket.on("call-leave", data => {
      //now write a disconnect for loop for call to remove the user.
      for (i = 0; i < inCall.length; i++) {
        if (socket.id == inCall[i].socketId) {
          //change this console log to add both users to a chat room, remove person[i] from the array
      console.log(`${socket.request.user.username} (${socket.id}) ended swerve call`);
      console.log(inCall)

      io.sockets.connected[socket.id].leave(inCall[i].room);

      io.in(inCall[i].room).clients((error, clients) => {
        if (error) throw error;
        console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
      });

      if(data.matched == true){
        console.log('matched true')
        //make a match notification so they don't think the user just DC'd on them
        io.to(socket.id).emit('matched', {match: inCall[i].peer});
        //add the user to "matches"
        userMatches.push({user: inCall[i].peer});
        console.log(socket.request.user.username)
        console.log(userMatches)
      }

      if(data.swerved == true){
        console.log('swerved true')
        //make a match notification so they don't think the user just DC'd on them
        io.to(socket.id).emit('swerved', {swerve: inCall[i].peer});
        //add the user to "matches"
        userSwerves.push({user: inCall[i].peer});
        console.log(socket.request.user.username)
        console.log(userSwerves)
      }

      io.to(socket.id).emit('disconnectRoom', {room: inCall[i].room, disconnector: socket.request.user.username});


      inCall.splice(i, 1);
      countdownTime = 0;
      break;
      } else {
        console.log('user not in swervecall')
      }

        }
        //gotta run it twice to get the other user
        for (i = 0; i < inCall.length; i++) {
          if (inCall[i].peerId == socket.id) {
            //change this console log to add both users to a chat room, remove person[i] from the array
        console.log(`${inCall[i].username} (${inCall[i].socketId}) was disconnected`);
        console.log(inCall)

        //make the peer leave :^(
        io.sockets.connected[inCall[i].socketId].leave(inCall[i].room);

        io.in(inCall[i].room).clients((error, clients) => {
          if (error) throw error;
          console.log(`Clients in this room are: ${clients}`); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
        });

        if(data.matched == true){
          console.log('matched true')
          //make a match notification so they don't think the user just DC'd on them
          io.to(inCall[i].socketId).emit('matched', {match: inCall[i].peer, sendBack:true})
          //next I gotta make it send back something so that I can build add matches to THEIR userMatches too.
        }

        if(data.swerved == true){
          console.log('swerved true')
          //make a match notification so they don't think the user just DC'd on them
          io.to(inCall[i].socketId).emit('swerved', {swerve: inCall[i].peer, sendBack:true})
          //next I gotta make it send back something so that I can build add matches to THEIR userMatches too.
        }

        io.to(inCall[i].socketId).emit('disconnectRoom', {room: inCall[i].room, disconnector: socket.request.user.username});

        inCall.splice(i, 1)
        break;
        } else {
          console.log('user no longer in swervecall')
        }

          }
    });

    //leave the searching waiting room. Keep this seperate from the call leave.
    socket.on("search-leave", () => {
      //now write a disconnect for loop for waiting to remove the user.
      for (i = 0; i < waiting.length; i++) {
        if (socket.id == waiting[i].socketId) {
          //change this console log to add both users to a chat room, remove person[i] from the array
      console.log(`${socket.request.user.username} (${socket.id}) left the waiting room`);
      console.log(waiting)
      waiting.splice(i, 1)
      break;
      } else {
        console.log('user not in swerve waiting room')
      }

        }
    });

    socket.on("informServerMatch", data => {
      userMatches.push({user: data.match});
      console.log(socket.request.user.username)
      console.log(userMatches)
    });

    socket.on("informServerSwerve", data => {
      userSwerves.push({user: data.swerve});
      console.log(socket.request.user.username)
      console.log(userSwerves)
    });




    socket.on("notification", data => {
      //Eh I'll work on this later. Actually I don't really need to console log this shit at all but it's fun in development.
      //maybe I'll do it in an ACP (admin control panel) API on the front end eventually
      if(data.to in connectedUsers && data.from == socket.request.user.username) {
        console.log(`${data.from} ${data.notificationType}ed ${data.to}`);
        for(let userSet of connectedUsers[data.to]) {
            io.to(userSet).emit("notification", {notificationType: data.notificationType, from: data.from, to: data.to});
        }
      }
    });

    socket.on("forceLogout", data => {
      //Eh I'll work on this later. Actually I don't really need to console log this shit at all but it's fun in development.
      //maybe I'll do it in an ACP (admin control panel) API on the front end eventually
      console.log(`${data.user} did a force logout`);

      if(data.user in connectedUsers) {
        for(let userSet of connectedUsers[data.user]) {
            io.to(userSet).emit("forceLogout", {user: socket.request.user.username});
        }
      }
    });

  });

};
