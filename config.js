//this is a config file! Change stuff to match your needs!

//port to listen on
port = 3000,

//App name (without the .com or whatever)
appName = "swerve"

//your site's name
domain = "swerve.dating",

//Database
database = `mongodb://localhost:27017/${appName}`;
//Your secret, make it secretive!
secret = 'cyberdating';

//I REMOVED THE PEM KEY SHIT FROM THE CONFIG!
//Since I'm recommending Nginx now, I suggest just using certbot's nginx bot. It'll take care of everything for you and save you a lot of time.
//I'll leave an nginx.txt file explaining how to do NGINX fuckery later.

//////////////////////////////////////////////////////////////////

//let's make it an exportable object
const config = {
port: port,
domain: domain,
db: database,
secret: secret,
//if you plan on statically hosting your frontend files with NGINX, check this true.
//Otherwise node will do it for you. The node devs don't recommend you hosting static files with their shit. They suggest doing it with nginx.
useNginx: true
}

module.exports = config;
