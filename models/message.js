const mongoose = require('mongoose');
const config = require('../config');

const MessageSchema = mongoose.Schema({
  notifcationType: String,
  date: String,
  from: String,
  username: String,
  body: String,
  identifier: String,
  createAt: {
    type: Date,
    default: Date.now(),
    expires: 2419200
}
});

const Message = module.exports = mongoose.model('Message', MessageSchema);

module.exports.addMessage = function(newMessage, callback){
      newMessage.save(callback)
    };

    //Delete Notications by ID function
    module.exports.deleteById = function(id, callback){
      Message.findByIdAndRemove(id, callback);
    };

    //get all Notifys with a certain Username
    module.exports.getUserByIdentifier = function(identifier, callback){
        const query = {identifier: identifier}
        Message.find(query, callback);
      };

      //get last Notifys with a certain Username... Used in combo with the User model's lastmessage array
      module.exports.getUserByIdentifierLast = function(identifier, callback){
          const query = {identifier: identifier}
          Message.find(query, callback).sort({_id:-1}).limit(1);
        };
