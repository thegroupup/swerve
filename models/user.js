const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config');
const uuid = require('uuid');

const UserSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  uuid: {
    type: String,
    required: true
  },
  created: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  username: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  bio: {
    type: String
  },
  location: {
    type: String
  },
  keep: {
    type: String
  },
  followers: [{user: String, _id: false}],
  following: [{user: String, _id: false}],
  matches: [{user: String, _id: false}],
  matchRequests: [{user: String, _id: false}],
  matchSends: [{user: String, _id: false}],
  swerves: [{user: String, _id: false}],
  lastMessage: [{with: String, user: String, date: String, identifier: String, _id: false}],
  blocked: [{user: String, _id: false}]
});

const User = module.exports = mongoose.model('User', UserSchema);

//Get user by ID
module.exports.getUserById = function(id, callback){
  User.findById(id, callback);
};

//Delete user by ID function
module.exports.deleteById = function(id, callback){
  User.findByIdAndRemove(id, callback);
};

//Get user by username
module.exports.getUserByUsername = function(username, callback){
  const query = {username: username}
  User.findOne(query, callback);
};

//Get user by uuid (Used for login and force logout. Yes, it's very important)
module.exports.getUserByUuid = function(uuid, callback){
  const query = {uuid: uuid}
  User.findOne(query, callback);
};

module.exports.addUser = function(newUser, callback){
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newUser.password, salt, (err, hash) => {
      if(err) throw err;
      newUser.password = hash;
      newUser.save(callback)
    });
  });
};

module.exports.comparePassword = function(candidatePassword, hash, callback){
  bcrypt.compare(candidatePassword, hash, (err, isMatch) => {
    if(err) throw err;
    callback(null, isMatch);
  });
};

module.exports.updatePassword = (id, newPassword, options, callback) => {
  let query = {_id: id};

  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash(newPassword, salt, (err, hash) => {
      if(err) throw err;
      let update = {
        password: hash
      };
      User.findOneAndUpdate(query, update, options, callback);
    });
  });
};


//Update User function. Stuff you'll allow to be updated after account creation.
module.exports.updateUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    name: user.name,
    bio: user.bio,
    avatar: user.avatar,
    location: user.location
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//This adds the user you followed to your following list, not to be confused with my next creation...
module.exports.followUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $push: {following: {user: user.target, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};
//This adds your name to the user you followed. I didn't know what else to call it tbh...
module.exports.addFollowUser = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $push: {followers: {user: user.username, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//add the other user to your match send list. See followerUser for reference
module.exports.requestMatch = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $push: {matchSends: {user: user.target, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//Add yourself to their match requests. See addFollowUser for reference.
module.exports.requestMatchOther = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $push: {matchRequests: {user: user.username, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//add the other user to your matches list. See followerUser for reference
module.exports.matchUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $push: {matches: {user: user.target, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//Add yourself to their matches list. See addFollowUser for reference.
module.exports.matchOther = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $push: {matches: {user: user.username, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.swerveUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $push: {swerves: {user: user.target, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.swerveUserOther = (id, user, options, callback) => {
  let query = {username: user.target};
	let update = {
    $push: {swerves: {user: user.username, date: user.date}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};





//THIS adds the LAST message someone sent to you to an array called "lastMessage". It'll be used with remove. I'll explain it in the next function
module.exports.addLatestMessage = (id, user, options, callback) => {
  const query = {username: user.username }
	let update = {
    $push: {lastMessage: {with: user.from, identifier: user.identifier}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//So this will remove what the above created. BASICALLY it's to make latest message someone sent you erase
//You'll put this BEFORE the add latest message. This will ensure there's only one latest per person.
//we'll use this for the message list contact type thing like facebook has and put the last child of the /m/messages with it
module.exports.removeLatestMessage = (id, user, options, callback) => {
  //use this query as an example for follows you check later.
  const query = {username: user.username, lastMessage: {with: user.from, identifier: user.identifier} }
	let update = {
    $pull: {lastMessage: { with: user.from, identifier: user.identifier}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//now we'll have to create the same thing but to the yourself so they both get updated
module.exports.addLatestMessageYourself = (id, user, options, callback) => {
  const query = {username: user.from }
	let update = {
    $push: {lastMessage: {with: user.username, identifier: user.identifier}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.removeLatestMessageYourself = (id, user, options, callback) => {
  const query = {username: user.from }
	let update = {
    $pull: {lastMessage: { with: user.username, identifier: user.identifier}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};


//This blocks the user. I'll create a block if statement for certain things in the user route and message route I suppose.
module.exports.blockUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $push: {blocked: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.unblockUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {blocked: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};


module.exports.unfollowUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {following: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.addUnfollowUser = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $pull: {followers: {user: user.username}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.unfollowerUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {followers: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.addUnfollowerUser = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $pull: {following: {user: user.username}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

//gotta copy paste the previous shit to make it work for matches now fml
module.exports.unmatchUser = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {matches: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.removeMatchOther = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $pull: {matches: {user: user.username}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.unmatchSend = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {matchSends: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.removeMatchSendOther = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $pull: {matchSends: {user: user.username}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.unmatchRequest = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    $pull: {matchRequests: {user: user.target}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};

module.exports.removeMatchRequestOther = (id, user, options, callback) => {
  const query = {username: user.target}
	let update = {
    $pull: {matchRequests: {user: user.username}}
	};
	User.findOneAndUpdate(query, update, options, callback);
};





//get all followers of a particular user. This will let us do a fancier list in the front end than just their names.
module.exports.getFollowersByUsername = function(followers, callback){
    const query = {followers: {user: followers.target}}
    User.find(query, 'username avatar bio', callback);
  };

  module.exports.getFollowingByUsername = function(following, callback){
      const query = {following: {user: following.target}}
      User.find(query, 'username avatar bio', callback);
    };


//this is for a force logout. Use username for everything from now on instead of _id
module.exports.updateId = (id, user, options, callback) => {
  let query = {_id: id};
	let update = {
    uuid: `${user.username}+${uuid.v4()}`
	};
	User.findOneAndUpdate(query, update, options, callback);
};
