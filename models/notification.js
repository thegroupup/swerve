const mongoose = require('mongoose');
const config = require('../config');

const NotifySchema = mongoose.Schema({
  notificationType: String,
  date: String,
  from: String,
  seen: Boolean,
  username: String,
  createAt: {
    type: Date,
    default: Date.now(),
    expires: 604800
}
});

const Notify = module.exports = mongoose.model('Notify', NotifySchema);

module.exports.addNotification = function(newNotify, callback){
      newNotify.save(callback)
    };

    //Delete Notify by ID function
    module.exports.deleteById = function(id, callback){
      Notify.findByIdAndRemove(id, callback);
    };

    //get all Notifys with a certain Username
    module.exports.getUserByUsername = function(username, callback){
        const query = {username: username}
        Notify.find(query, callback);
      };

      //get all unseen Notifys with a certain Username. Used for navbar.
      module.exports.getUnseenByUsername = function(username, callback){
          const query = {username: username, seen: false}
          Notify.find(query, callback);
        };

        //mark all notifications as seen. Will trigger when they click the bell.
        module.exports.markAllSeen = (username, options, callback) => {
          let query = {username: username, seen: false}
        	let update = {
            seen: true
        	};
        	Notify.findOneAndUpdate(query, update, options, callback);
        };

//remove notification
module.exports.removeFollowNotification = (id, follow, options, callback) => {
  console.log(follow);
  let query = { from: follow.username, username: follow.target, notificationType: "Follow" };
  Notify.remove(query, options, callback);
};
